const AgedBrie = 'Aged Brie'
const BackstagePass = 'Backstage passes to a TAFKAL80ETC concert'
const Conjured = 'Conjured'
const Sulfuras = 'Sulfuras, Hand of Ragnaros'

module.exports = {
  AgedBrie,
  BackstagePass,
  Conjured,
  Sulfuras
}
