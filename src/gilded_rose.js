const { AgedBrie, BackstagePass, Conjured, Sulfuras } = require('./constants')

const ConjuredRegex = new RegExp(Conjured)

class Item {
  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

class Shop {
  constructor(items = []) {
    this.items = items;
  }

  passTime() {
    this.items.forEach((item) => {
      if (item.name === Sulfuras) {
        return
      }

      updateQuality(item)

      item.sellIn -= 1

      if (item.sellIn < 0) {
        updateQuality(item)
      }
    })

    return this.items;
  }
}

function updateQuality(item) {
  if (item.name === BackstagePass) {
    if (item.sellIn < 0) {
      return item.quality = 0
    }

    const findChange = (sellIn) => {
      if (sellIn < 6) return 3
      if (sellIn < 11) return 2
      return 1
    }

    const change = findChange(item.sellIn)

    increaseQuality(item, change)
    return
  }

  if (item.name === AgedBrie) {
    increaseQuality(item)
    return
  }

  if (ConjuredRegex.test(item.name)) {
    decreaseQuality(item, 2)
    return
  }

  decreaseQuality(item)
}

function decreaseQuality(item, change = 1) {
  item.quality = Math.max(0, item.quality - change)
}

function increaseQuality(item, change = 1) {
  item.quality = Math.min(50, item.quality + change)
}

module.exports = {
  Item,
  Shop
}
