const { Shop, Item } = require('../src/gilded_rose')

describe('Gilded Rose', () => {
  const subject = (name, sellIn, quality) => {
    const item = new Item(name, sellIn, quality)
    const shop = new Shop([item])

    return shop.passTime()[0]
  }

  describe('passing time', () => {
    describe('basic items', () => {
      const name = 'Simple Dagger'

      it('reduces sellIn by 1', () => {
        const result = subject(name, 0, 1)

        expect(result.sellIn).toEqual(-1)
      })

      it('degrades quality for basic items', () => {
        const result = subject(name, 1, 1)

        expect(result.quality).toEqual(0)
        expect(result.sellIn).toEqual(0)
      })

      it('has minimum quality of 0', () => {
        const result = subject(name, 2, 0)

        expect(result.quality).toEqual(0)
        expect(result.sellIn).toEqual(1)
      })

      it('degrades twice as fast when sellIn goes negative', () => {
        const result = subject(name, 0, 5)

        expect(result.quality).toEqual(3)
        expect(result.sellIn).toEqual(-1)
      })
    })

    describe('special items', () => {
      describe('aged brie', () => {
        const name = 'Aged Brie'

        it('increases in quality as it gets older', () => {
          const result = subject(name, 1, 1)

          expect(result.quality).toEqual(2)
          expect(result.sellIn).toEqual(0)
        })

        it('has maximum quality of 50', () => {
          const result = subject(name, 2, 50)

          expect(result.quality).toEqual(50)
          expect(result.sellIn).toEqual(1)
        })

        it('doubles the rate of quality improvement after sellIn goes negative', () => {
          const result = subject(name, 0, 10)

          expect(result.quality).toEqual(12)
          expect(result.sellIn).toEqual(-1)
        })
      })

      describe('sulfuras', () => {
        const name = 'Sulfuras, Hand of Ragnaros'

        it('never decreases in quality', () => {
          const result = subject(name, 1, 45)

          expect(result.quality).toEqual(45)
        })

        it('never decreases sellIn date', () => {
          const result = subject(name, 1, 45)

          expect(result.sellIn).toEqual(1)
        })
      })

      describe('backstage passes', () => {
        const name = 'Backstage passes to a TAFKAL80ETC concert'

        it('increases in quality by 1 while sellIn is greater than 10', () => {
          const result = subject(name, 11, 5)

          expect(result.quality).toEqual(6)
          expect(result.sellIn).toEqual(10)
        })

        it('increases in quality by 2 while sellIn is 10 or less', () => {
          const result = subject(name, 10, 5)

          expect(result.quality).toEqual(7)
          expect(result.sellIn).toEqual(9)
        })

        it('increases in quality by 3 while sellIn is 5 or less', () => {
          const result = subject(name, 5, 5)

          expect(result.quality).toEqual(8)
          expect(result.sellIn).toEqual(4)
        })

        it('still increases in quality by 3 on the day of the concert', () => {
          const result = subject(name, 1, 5)

          expect(result.quality).toEqual(8)
          expect(result.sellIn).toEqual(0)
        })

        it('becomes worthless after the concert', () => {
          const result = subject(name, 0, 5)

          expect(result.quality).toEqual(0)
          expect(result.sellIn).toEqual(-1)
        })
      })

      describe('conjured items', () => {
        const name = 'Conjured Mana Cake'

        it('degrades in quality twice as fast as normal items', () => {
          const result = subject(name, 5, 5)

          expect(result.quality).toEqual(3)
          expect(result.sellIn).toEqual(4)
        })

        it('degrades in quality twice as fast as normal items beyond the sell deadline', () => {
          const result = subject(name, 0, 5)

          expect(result.quality).toEqual(1)
          expect(result.sellIn).toEqual(-1)
        })
      })
    })
  })
})
